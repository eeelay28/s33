// GET method to retrieve all the to do list items 
async function fetchData(){
	let result = await fetch("https://jsonplaceholder.typicode.com/todos");
	let json = await result.json();
	// 3. GET method to retrieve all the to do list items 
	console.log(json);
	let allTitles = json.map((json) => json.title)
	// 4. title by map method
	console.log(allTitles);
}
fetchData();

// 5. Get method to retrieve a single to do list
fetch("https://jsonplaceholder.typicode.com/todos/99")
.then((response) => response.json())
.then((json) => console.log(json))

// 6. Print message in console from #5
fetch("https://jsonplaceholder.typicode.com/todos/99")
.then((response) => response.json())
.then((json) => {
	console.log(`${json.title} and ${json.completed}`)
})

// #7
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userId: 8,
		completed: true,
		title: "Created post"
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// #8
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userId: 8,
		completed: true,
		title: "updated to do list"
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// #9
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "new title",
		description: "new descrition",
		status: "pending",
		dateComplted: "pending",
		userId: 8
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// #10
fetch("https://jsonplaceholder.typicode.com/todos/99", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "patch post"
		// title: request.body.title
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// #11
fetch("https://jsonplaceholder.typicode.com/todos/99", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: "true",
		date_completed: "08-26-2022"
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// #12
fetch("https://jsonplaceholder.typicode.com/todos/99", {
	method: "DELETE"
})
